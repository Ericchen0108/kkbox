from src.organization import Organization

def test_create_organization():
    data = {
        "id": 1,
        "parent_id": 0,
        "organization_code": "president-office",
        "organization_name": "President Office",
        "supervisor_id": "0101001",
        "is_active": "true"
    }
    
    organization = Organization(
        data['id'],
        data['organization_code'],
        data['organization_name'],
        data['is_active']
    )
    
    assert organization.organization_code == "president-office"