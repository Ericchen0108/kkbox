from src.employee import Employee

def test_create_employee():
    data = {
        "id": 1,
        "employee_no": "0101001",
        "employee_name": "Bob",
        "email": "bob@mail",
        "onboarding_date": "20200101",
        "organization_id": "president-office",
        "job_title": "President",
        "gender": "M"
    }
    employee = Employee(
        data['id'], 
        data['employee_no'], 
        data['employee_name'], 
        data['email'], 
        data['onboarding_date'],
        data['job_title'],
        data['gender'],
        data['organization_id']
    )
    assert employee.id == 1
