import json
from organizationList import OrganizationList
from employeeList import EmployeeList

class Orgchart():
    
    def __init__(self, organization_json, employees_json): 
        
        self.organization = OrganizationList(self.read_json(organization_json))
        self.employee = EmployeeList(self.read_json(employees_json), self.organization)
        
        
    def read_json(self, file):
        with open(file) as f:
            data = json.load(f)
        return data
