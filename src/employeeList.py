from employee import Employee

class EmployeeList():
    def __init__(self, employee_json, organization):
        self.organization = organization
        self.employee_list = [Employee(
            employee['id'], 
            employee['employee_no'], 
            employee['employee_name'], 
            employee['email'], 
            employee['onboarding_date'], 
            employee['job_title'], 
            employee['gender'], 
            self.organization.get(employee['organization_id'])
            ) for employee in employee_json
        ]
        
    def list(self):
        return self.employee_list
    
    def get(self, params):
        for employee in self.employee_list:
            if (employee.id == params or 
            employee.employee_no == params or 
            employee.employee_name == params or 
            employee.email == params or 
            employee.onboarding_date == params or 
            employee.job_title == params or 
            employee.gender == params):
                return employee
            
    def update_organizations(self):
        pass