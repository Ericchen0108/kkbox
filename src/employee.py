class Employee():
    def __init__(self, 
                id, 
                employee_no, 
                employee_name, 
                email, 
                onboarding_date, 
                job_title, gender, 
                organization
    ):
        self.id = id
        self.employee_no = employee_no
        self.employee_name = employee_name
        self.email = email
        self.onboarding_date = onboarding_date
        self.job_title = job_title
        self.gender = gender
        self.organization = organization
        self.supervisor = ""
        self.organizations = []
