class Organization():

    def __init__(self, id, organization_code, organization_name, is_active):
        self.id = id
        self.organization_code = organization_code
        self.organization_name = organization_name
        self.is_actuve = is_active
        self.parent = ""
        self.supervisor = ""
        self.employees = []
