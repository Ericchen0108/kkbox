from organization import Organization

class OrganizationList():
    def __init__(self, organization_json):
        self.organization_list = []
        for org in organization_json:
            organization = Organization(
                org['id'], 
                org['organization_code'], 
                org['organization_name'], 
                org['is_active']
            )
            self.organization_list.append(organization)

        self.update_parent(organization_json)
                
    def list(self):
        return self.organization_list
    
    def get(self, params):
        for organization in self.organization_list:
            if (organization.id == params or 
                organization.organization_code == params 
                or organization.organization_name == params):
                return organization
            
    def update_parent(self, organization_json):
            for org in organization_json:
                if org['parent_id'] != 0:
                    self.get(org['id']).parent = self.get(org['parent_id'])
                    
    def update_supervisor(self):
        pass
    
    def update_employees(self):
        pass
